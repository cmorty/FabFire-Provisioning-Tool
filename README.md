FabFire Provisioning Tool
===
Tool for provisioning new cards for use with the FabAccess card system.
Using the cards requires a configured instance of the [fabfire adapter](https://gitlab.com/fabinfra/fabaccess/fabfire_adapter).

# Usage
## Provisioning
1. Run the following command while replaceing `--space`, `--instance` and `--contact` with your own values and setting `--token` to the users username:
    ```shell
    cargo run -- --space "innovisionlab" --instance fabaccess.innovisionlab.de --contact https://innovisionlab.de/lostandfound --token "Testuser" 
    ```
    You can supply your own keys and Application ID with the appropriate cmdline arguments, view `--help` for more information.

3. Add the provided `cardkey` from the output to the users entry in users.toml 

## Formating Card
```shell
cargo run -- --format
```
Formats the card, deleting all files and keys.

use std::ffi::CString;
use desfire::Card as CardTrait;
use desfire::error::Error;
use desfire::error::Error::CardError;
use desfire::iso7816_4::apducommand::APDUCommand;
use desfire::iso7816_4::apduresponse::APDUResponse;
use pcsc::{Card, Context, MAX_BUFFER_SIZE, Protocols, Scope, ShareMode};

pub struct PCSCCard {
    ctx: Context,
    reader: CString,
    card: Option<Card>,
}

impl PCSCCard {
    pub fn new() -> Result<PCSCCard, pcsc::Error> {
        // Establish a PC/SC context.
        let ctx = match Context::establish(Scope::User) {
            Ok(ctx) => ctx,
            Err(err) => {
                return Err(err);
            }
        };

// List available readers.
        let mut readers_buf = [0; 2048];
        let mut readers = match ctx.list_readers(&mut readers_buf) {
            Ok(readers) => readers,
            Err(err) => {
                return Err(err)
            }
        };

// Use the first reader.
        let reader = match readers.next() {
            Some(reader) => {
                println!("selected reader: {:?}", reader);
                reader
            },
            None => {
                return Err(pcsc::Error::NoReadersAvailable);
            }
        };

        Ok(PCSCCard {
            ctx,
            reader: CString::from(reader),
            card: None
        })
    }
}

impl CardTrait for PCSCCard {
    fn connect(&mut self) -> Result<(), Error> {
        self.card = match self.ctx.connect(&self.reader, ShareMode::Shared, Protocols::ANY) {
            Ok(card) => Some(card),
            Err(err) => {
                eprintln!("Failed to connect to card {} on reader {:?}", err, self.reader);
                return Err(CardError)
            }
        };

        return Ok(())
    }

    fn disconnect(&mut self) -> Result<(), Error> {
        Ok(())
    }

    fn transmit(&self, apdu_cmd: APDUCommand) -> Result<APDUResponse, Error> {
        let apdu = Vec::<u8>::try_from(apdu_cmd).unwrap();
        let mut rapdu_buf = [0; MAX_BUFFER_SIZE];
        let rapdu = match self.card.as_ref().as_ref().unwrap().transmit(apdu.as_slice(), &mut rapdu_buf) {
            Ok(rapdu) => rapdu,
            Err(err) => {
                eprintln!("Failed to transmit APDU command to card: {}", err);
                return Err(CardError)
            }
        };
        return Ok(APDUResponse::new(rapdu))
    }
}

